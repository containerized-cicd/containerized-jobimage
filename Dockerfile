FROM registry.gitlab.com/nmeisenzahl/dockerfiles/kubectl:latest

LABEL maintainer="nico@meisenzahl.org" \
org.label-schema.vcs-url="https://gitlab.com/gitlab-commit-demo"

RUN apk add --update --no-cache bash openssl \
&& curl -L https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 -o ./helm.sh \
&& chmod +x ./helm.sh \
&& ./helm.sh \
&& rm -rf ./helm.sh \
&& apk del --no-cache bash openssl

ENTRYPOINT ["helm"]
CMD ["--help"]
